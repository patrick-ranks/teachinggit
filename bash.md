# Bash in 2 slides

**List files in current directory**
```bash
$ ls
```	

**Which directory am I in?**
```bash
$ pwd
```
**Make a folder:**
```bash
$ mkdir myfolder
```

**Make a file:**    
Note: you can make files how ever you wish like normal: Eg by saving a document etc  Here are just 2 convenient ways of creating and editing text files when working in the bash
- The command 'touch' will simply create an empty file with the name you specify
- The command 'nano' will open a file with the name you give it.  Nano is a text editing program, to exit this file press ctrl+x and selecy 'y' or 'n' if you want to save it or not
```bash
$ touch ElephantFile
$ nano LionFile
```


**Change to another directory**
```bash
$ cd 
```
* Go to a higher directory:	
```bash
    $ cd ../
```
* Go to a subdirectory:		
```bash
    $ cd subdir1/subsubdir2
```
* Go to an absolute path:	
```bash
    $ cd /home/lskywalker
```

**Copy Files**
```bash
$ cp File1.txt	CopyofFile1.txt

**Copy Directories (folders)**
```bash
$ cp -r Folder1/ CopyOfFolder1
```


**Move/Rename a File or Folder**
$ mv < File or Folder > < Destination >

* Move to a higher directory:	
```bash
    $ mv File1 ../
    $ mv folder1 ../
```
* Move to a subdirectory:	
```bash
    $ mv File1 Subdir/
    $ mv folder1 Subdir/
```
* Rename:
```bash
    $ mv File1 File1_Renamed
    $ mv Dog/ Cat/
```

**Exit the terminal:**
* Type exit and press enter
or
* Press Ctrl+d


